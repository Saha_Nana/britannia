class AdmissionsController < ApplicationController
  before_action :set_admission, only: [:show, :edit, :update, :destroy]
  layout "doc_patients",:only => [ :admission_doc, :admission_vitals]
  # GET /admissions
  # GET /admissions.json
  def index

    @admission = Admission.new
    @add_a_room = Admission.new
    @a_discharge_note = Admission.new
    @clinic_document = ClinicDocument.new

    @admissions = Admission.joins("INNER JOIN patients ON patients.id = admissions.patient_id
                    INNER JOIN users ON users.id = admissions.user_id").select('surname,other_names,admission_note,admissions.patient_id,fullname,admissions.created_at,admissions.room_no').where(status: 1)
  end

  def nurse_admission_list
    @admission = Admission.new
    @add_a_room = Admission.new
    @a_discharge_note = Admission.new

    @admissions = Admission.joins("INNER JOIN patients ON patients.id = admissions.patient_id
                    INNER JOIN users ON users.id = admissions.user_id").select('surname,other_names,admission_note,admissions.patient_id,fullname,admissions.created_at,admissions.room_no').where(status: 1)
  end

  def discharged_list

    @admissions = Admission.joins("INNER JOIN patients ON patients.id = admissions.patient_id
                    INNER JOIN users ON users.id = admissions.user_id").select('surname,other_names,admission_note,admissions.patient_id,fullname,admissions.created_at,admissions.room_no').where(status: 0)
  end
  
  def discharged_list_nurses
    @admissions = Admission.joins("INNER JOIN patients ON patients.id = admissions.patient_id
                    INNER JOIN users ON users.id = admissions.user_id").select('surname,other_names,admission_note,admissions.patient_id,fullname,admissions.created_at,admissions.room_no').where(status: 0)
 
  end

  # GET /admissions/1
  # GET /admissions/1.json
  def show
  end

  # GET /admissions/new
  def new
    @admission = Admission.new
  end

  # GET /admissions/1/edit
  def edit
  end

  def admission_doc

    @admission = Admission.new
    patient_id =params[:id]
    @patient_id = patient_id

    @patient_records = Patient.find_by(id: patient_id)
    @admissions = Admission.joins("INNER JOIN patients ON patients.id = admissions.patient_id
                    INNER JOIN users ON users.id = admissions.user_id").select('surname,other_names,admission_note,admissions.patient_id,fullname,admissions.created_at').where(patient_id: patient_id )
  end

  def patient_admission

    @admission = Admission.new

    patient_id=params[:id]
    @patient_id = patient_id

    logger.info "Lets see medical id #{@patient_id}"

    respond_to do |format|

      format.js
      format.html
    end
  end

  def view_note

    @admission = Admission.new
    created_at=params[:id]

    @documents = Admission.where(created_at: created_at)
    logger.info "document1: #{@documents.inspect}"

    respond_to do |format|

      format.html
      format.js
    end

  end

  def view_discharge_note

    @admission = Admission.new
    created_at=params[:id]

    @documents = Admission.where(created_at: created_at)
    logger.info "document1: #{@documents.inspect}"

    respond_to do |format|

      format.html
      format.js
    end

  end

  def add_room

    @add_a_room = Admission.new
    @created_at=params[:id]

    logger.info "LETS SEE THE DATE SELECTED #{@created_at}"

    @admission_records = Admission.find_by(created_at: @created_at)

    logger.info "LETS SEE Admission object #{@admission_records}"
    logger.info "LETS SEE Admission object #{@admission_records.inspect}"

  end
  
  def edit_room

    @add_a_room = Admission.new
    @created_at=params[:id]

    logger.info "LETS SEE THE DATE SELECTED #{@created_at}"

    @admission_records = Admission.find_by(created_at: @created_at)
    @room_no = @admission_records.room_no
    logger.info "LETS SEE ROom no #{@room_no}"
    logger.info "LETS SEE Admission object #{@admission_records.inspect}"

  end

  def add_a_room

    @room_no = params[:admission][:room_no]
    @created_at = params[:admission][:created_at]

    logger.info "LETS SEE THE DATE SELECTED #{@created_at}"
    logger.info "LETS SEE THE ROom number #{@room_no}"

    @admission_records = Admission.find_by(created_at: @created_at)

    logger.info "LETS SEE Admission object #{@admission_records}"
    logger.info "LETS SEE Admission object #{@admission_records.inspect}"
    logger.info "LETS SEE Admission ID #{@admission_records.id}"

    Admission.update(@admission_records.id, :room_no => @room_no)

    respond_to do |format|

      format.html  { redirect_to admissions_path, notice: 'Room number was added successfully.' }
      format.js
    end

  end

  # def discharge_patient
  #   created_at =params[:id]
  #   @admission_records = Admission.find_by(created_at: created_at)

  #   if Admission.update(@admission_records.id, :status => 0)
  #     redirect_to admissions_path, notice: 'Patient Discharged.'

  #   end
  # end

  def discharge_note
    @a_discharge_note = Admission.new
    @created_at=params[:id]

    logger.info "LETS SEE THE DATE SELECTED #{@created_at}"

    @admission_records = Admission.find_by(created_at: @created_at)

    logger.info "LETS SEE Admission object #{@admission_records}"
    logger.info "LETS SEE Admission object #{@admission_records.inspect}"
  end

  def a_discharge_note
    @discharge_note = params[:admission][:discharge_note]
    @created_at = params[:admission][:created_at]

    logger.info "LETS SEE THE DATE SELECTED #{@created_at}"
    logger.info "LETS SEE THE ROom number #{@discharge_note}"

    @admission_records = Admission.find_by(created_at: @created_at)

    Admission.update(@admission_records.id, :discharge_note => @discharge_note,:status => 0)

    respond_to do |format|

      format.html  { redirect_to admissions_path, notice: 'Patient Discharged Successfully.' }
      format.js
    end
  end

  def admission_vitals

    @assign_patient = AssignPatient.new
    @clinic_document = ClinicDocument.new

    patient_id =params[:id]
    @patient_id = patient_id

    @patient_records = Patient.find_by(id: patient_id)

    logger.info "Lets see patient_id id #{@patient_id}"
    logger.info "Lets see  Patient name #{@patient_records.surname } #{@patient_records.other_names }"
    if (defined?(patient_id)).nil?

      @b_records = BasicMedRecord.find_by(patient_id: patient_id)
      @blood_pressure = @b_records.blood_pressure
      @pulse = @b_records.pulse
      @respiratory = @b_records.respiratory_rate
      @temperature = @b_records.temp
      @patient_id = @b_records.patient_id

      patient_id = @patient_id

      @basic_med_id = @b_records.id
      basic_med_id = @basic_med_id

      logger.info "Lets see medical id #{patient_id}"

      @clinic_documents = ClinicDocument.joins("INNER JOIN patients ON patients.id = clinic_documents.patient_id
                    INNER JOIN users ON users.id = clinic_documents.user_id INNER JOIN basic_med_records ON basic_med_records.id = clinic_documents.basic_medical_record_id").select('surname,other_names,patients.mobile_number,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,basic_med_records.id,complaints,symptoms,vital_signs,clinic_exams,final_diagnosis,investigations,treatment_plans,card_no,clinic_documents.created_at,clinic_documents.patient_id,clinic_documents.id').where(patient_id: patient_id,status: 0 ).order('id desc')

      logger.info "CLINIC DOCUMENTS: #{@clinic_documents}"

      @document1 = ClinicDocument.find_by(id: patient_id)
      logger.info "document1: #{@document1}"
      logger.info "complaints: #{@document1.complaints}"

    else

      logger.info "XXXXXXXXXXXXXXXXXXXXXX"

      @clinic_documents = ClinicDocument.joins("INNER JOIN patients ON patients.id = clinic_documents.patient_id
                    INNER JOIN users ON users.id = clinic_documents.user_id").select('surname,other_names,patients.mobile_number,fullname,occupation,address,dob,patients.user_id,complaints,symptoms,vital_signs,clinic_exams,final_diagnosis,investigations,treatment_plans,card_no,clinic_documents.created_at,clinic_documents.patient_id,clinic_documents.id').where(patient_id: patient_id,status: 0 ).order('id desc')

    end

    respond_to do |format|

      format.html
      format.js
    end

  end

  def doctor_admission_vitals_doc

    @clinic_document = ClinicDocument.new

    patient_id=params[:id]
    @patient_id = patient_id

    logger.info "Lets see medical id #{@patient_id}"

    @b_records = BasicMedRecord.find_by(patient_id: patient_id)

    @blood_pressure = @b_records.blood_pressure
    @pulse = @b_records.pulse
    @respiratory = @b_records.respiratory_rate
    @temperature = @b_records.temp
    @patient_id = @b_records.patient_id
    @complaints = @b_records.complaints

    @vital_signs = "Respiratory Rate: " + @b_records.respiratory_rate + " " + "Pulse: " +  @b_records.pulse + " " + "Blood Pressure: " +  @b_records.blood_pressure + " " + "Temperature: " +  @b_records.temp

    logger.info "Vital signs: #{@vital_signs}"

    if (defined?(@b_records)).nil?
      @ass_p_records = AssignPatient.find_by(patient_id: patient_id)

      AssignPatient.update(@ass_p_records.id, :status => 1)

      logger.info "------------------------------------------"
      logger.info "Lets see the b records"
      logger.info "basic medical record: #{@b_records.inspect}"
      logger.info "------------------------------------------"

      @blood_pressure = @b_records.blood_pressure
      @pulse = @b_records.pulse
      @respiratory = @b_records.respiratory_rate
      @temperature = @b_records.temp
      @patient_id = @b_records.patient_id

      patient_id = @patient_id

      @basic_med_id = @b_records.id
      basic_med_id = @basic_med_id
      logger.info "patient id: #{@patient_id}"
      logger.info " basic_med_id: #{@basic_med_id}"
      logger.info "   @b_records.blood_pressure: #{ @blood_pressure}"

    else

      logger.info " LIEEEEESSSSS"

    end

    respond_to do |format|

      format.js
      format.html
    end
  end

  def view_doctor_admission_doc

    @assign_patient = AssignPatient.new
    created_at=params[:id]

    @documents = ClinicDocument.where(created_at: created_at)
    logger.info "document1: #{@documents.inspect}"

    respond_to do |format|

      format.html
      format.js
    end

  end

  # POST /admissions
  # POST /admissions.json
  def create
    patient_id =  params[:admission][:patient_id]
    admission_days = params[:admission][:admission_days]
    logger.info "Lets see PATIENT ID #{patient_id}"
    logger.info "Lets see ADMISION DAYS #{admission_days}"

    service_id = 8
    @services_data = ServicesInventory.find_by(id: service_id)
    price = @services_data.price.to_f * admission_days.to_i
    logger.info "Lets see PRICE #{price}"

    if BillTemp.where(patient_id: patient_id).exists?
      @id = BillTemp.find_by(patient_id: patient_id)
      @bills = BillHistory.new({:service_id => service_id,:patient_id => patient_id, :quantity => admission_days, :price => price ,:status=> 0})
      @bills_temp = BillTemp.update(@id.id, :created_at => Time.now )
    @bills.save

    else
      @bills = BillHistory.new({:service_id => service_id,:patient_id => patient_id, :quantity => admission_days, :price => price ,:status=> 0})
      @bills_temp = BillTemp.new({:patient_id => patient_id, :status=> 0})
    @bills.save
    @bills_temp.save
    end

    @admission = Admission.new(admission_params)

    respond_to do |format|
      if @admission.save
        format.html { redirect_to admission_doc_path(:id => patient_id), notice: 'Admission was successfully created.' }
        format.json { render :show, status: :created, location: @admission }
      else
        format.html { render :new }
        format.json { render json: @admission.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admissions/1
  # PATCH/PUT /admissions/1.json
  def update
    respond_to do |format|
      if @admission.update(admission_params)
        format.html { redirect_to admissions_path, notice: 'Admission was successfully updated.' }
        format.json { render :show, status: :ok, location: @admission }
      else
        format.html { render :edit }
        format.json { render json: @admission.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admissions/1
  # DELETE /admissions/1.json
  def destroy
    @admission.destroy
    respond_to do |format|
      format.html { redirect_to admissions_url, notice: 'Admission was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_admission
    @admission = Admission.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def admission_params
    params.require(:admission).permit(:patient_id, :admission_note, :user_id, :personnel_id, :status, :room_no, :admission_days)
  end
end

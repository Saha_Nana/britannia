class AssignPatientsController < ApplicationController
  before_action :set_assign_patient, only: [:show, :edit, :update, :destroy]

  layout "doc_patients",:only => [ :docs]
  # :only => [ :show, :edit, :update, :destroy]
  # GET /assign_patients
  # GET /assign_patients.json
  def index

    @vital_record = BasicMedRecord.new
    #@assign_patients = AssignPatient.joins(:patients).select('surname,other_names,mobile_number,occupation,address,dob,patients.user_id').group('patient_id').where(nurse_id: current_user.id )
    @clinic_document = ClinicDocument.new

    @assign_patients= AssignPatient.joins("INNER JOIN patients ON patients.id = assign_patients.patient_id
                  INNER JOIN users ON users.id = assign_patients.user_id").select('surname,other_names,patients.mobile_number,fullname,occupation,address,dob,patients.user_id,patient_id,assign_patients.id,assign_patients.status,basic_medical_record_id,personnel_id,users.role_id,card_no,assign_patients.created_at').where(personnel_id: current_user.role_id).order('id desc')

    @assign_to_doctor = AssignPatient.new
    @assign_patient = AssignPatient.new
    @lab_request = LabRequest.new
    @medication_request = MedicationRequest.new

  end

  # GET /assign_patients/1
  # GET /assign_patients/1.json
  def show
  end

  # GET /assign_patients/new
  def new
    @assign_patient = AssignPatient.new

    @patient = Patient.find(params[:id])

    session[:return_to] ||= request.referer
    @record = Patient.find_by(id: patient_id)

    logger.info "Lets see the records"
    logger.info "record: #{@record.inspect}"
    logger.info "patient id: #{@record.id}"
    p "record: #{@record.inspect}"
    logger.info "---------------------------------------------"
    logger.info "---------------------------------------------"
    @patient = Patient.new
  end

  # GET /assign_patients/1/edit
  def edit
  end

  def show_details
    patient_id=params[:id]

    @patients1 = Patient.find_by(id: patient_id)
    logger.info "patients1: #{@patients1}"
    @patients1.other_names
    logger.info "other_names: #{@patients1.other_names}"
  end

  def basic_medical
    @vital_record = BasicMedRecord.new
    @created_at=params[:id]

    logger.info "Lets see id para #{@created_at}"
    patient_record = AssignPatient.find_by(created_at: @created_at)
    @patient_id =  patient_record.patient_id
    logger.info "Lets see the patient id in basic medical is #{@patient_id}"

    @assign_patient = AssignPatient.new

    respond_to do |format|

      format.html
      format.js
    end
  end
  
  def basic_vitals
   @vital_record = BasicMedRecord.new
 
   patient_id  = params[:basic_med_record][:patient_id]
   blood_pressure  = params[:basic_med_record][:blood_pressure]
   pulse  = params[:basic_med_record][:pulse]
   temp  = params[:basic_med_record][:temp]
   respiratory_rate  = params[:basic_med_record][:respiratory_rate]
   user_id  = params[:basic_med_record][:user_id]
   status  = params[:basic_med_record][:status]
   complaints  = params[:basic_med_record][:complaints]
   admission_status  = params[:basic_med_record][:admission_status]
   @info = BasicMedRecord.new({:patient_id => patient_id,:blood_pressure => blood_pressure,:pulse => pulse,:temp => temp,:respiratory_rate => respiratory_rate,:user_id => user_id,:status => status,:complaints => complaints, :admission_status => admission_status})
   @info.save
   
   respond_to do |format|
      
        format.html { redirect_to basic_med_records_path, notice: 'Patient Vitals Taken Successfully ' }
        format.js
    end
    
  end
  
  

  def clinic_documentation

    @assign_patient = AssignPatient.new
    @clinic_document = ClinicDocument.new

    patient_id=params[:id]
    @patient_id = patient_id

    logger.info "Lets see medical id #{@patient_id}"

    @b_records = BasicMedRecord.find_by(patient_id: patient_id)

    @blood_pressure = @b_records.blood_pressure
    @pulse = @b_records.pulse
    @respiratory = @b_records.respiratory_rate
    @temperature = @b_records.temp
    @patient_id = @b_records.patient_id
    @complaints = @b_records.complaints

    @vital_signs = "Respiratory Rate: " + @b_records.respiratory_rate + " " + "Pulse: " +  @b_records.pulse + " " + "Blood Pressure: " +  @b_records.blood_pressure + " " + "Temperature: " +  @b_records.temp

    logger.info "Vital signs: #{@vital_signs}"

    if (defined?(@b_records)).nil?
      @ass_p_records = AssignPatient.find_by(patient_id: patient_id)

      AssignPatient.update(@ass_p_records.id, :status => 1)

      logger.info "------------------------------------------"
      logger.info "Lets see the b records"
      logger.info "basic medical record: #{@b_records.inspect}"
      logger.info "------------------------------------------"

      @blood_pressure = @b_records.blood_pressure
      @pulse = @b_records.pulse
      @respiratory = @b_records.respiratory_rate
      @temperature = @b_records.temp
      @patient_id = @b_records.patient_id

      patient_id = @patient_id

      @basic_med_id = @b_records.id
      basic_med_id = @basic_med_id
      logger.info "patient id: #{@patient_id}"
      logger.info " basic_med_id: #{@basic_med_id}"
      logger.info "   @b_records.blood_pressure: #{ @blood_pressure}"

    else

      logger.info " LIEEEEESSSSS"

    end

    respond_to do |format|

      format.js
      format.html
    end
  end

  def view_documentation

    @assign_patient = AssignPatient.new
    created_at=params[:id]

    @documents = ClinicDocument.where(created_at: created_at)
    logger.info "document1: #{@documents.inspect}"

    respond_to do |format|

      format.html
      format.js
    end

  end

  def assign_lab
    @assign_patient = AssignPatient.new
    @lab_request = LabRequest.new
    basic_med_id=params[:id]

    @b_records = BasicMedRecord.find_by(id: basic_med_id)

    @patient_id = @b_records.patient_id
    patient_id = @patient_id

    @basic_med_id = @b_records.id
    basic_med_id = @basic_med_id

    respond_to do |format|

      format.html
      format.js
    end

  end

  def assign_med
    @assign_patient = AssignPatient.new
    @medication_request = MedicationRequest.new
    basic_med_id=params[:id]

    @b_records = BasicMedRecord.find_by(id: basic_med_id)

    @patient_id = @b_records.patient_id
    patient_id = @patient_id

    @basic_med_id = @b_records.id
    basic_med_id = @basic_med_id

    respond_to do |format|

      format.html
      format.js
    end
  end

  def docs

    @assign_patient = AssignPatient.new
    @clinic_document = ClinicDocument.new

    patient_id =params[:id]
    @patient_id = patient_id

    @patient_records = Patient.find_by(id: patient_id)

    logger.info "Lets see patient_id id #{@patient_id}"
    logger.info "Lets see  Patient name #{@patient_records.surname } #{@patient_records.other_names }"
    if (defined?(patient_id)).nil?

      @b_records = BasicMedRecord.find_by(patient_id: patient_id)
      @blood_pressure = @b_records.blood_pressure
      @pulse = @b_records.pulse
      @respiratory = @b_records.respiratory_rate
      @temperature = @b_records.temp
      @patient_id = @b_records.patient_id

      patient_id = @patient_id

      @basic_med_id = @b_records.id
      basic_med_id = @basic_med_id

      logger.info "Lets see medical id #{patient_id}"

      @clinic_documents = ClinicDocument.joins("INNER JOIN patients ON patients.id = clinic_documents.patient_id
                    INNER JOIN users ON users.id = clinic_documents.user_id INNER JOIN basic_med_records ON basic_med_records.id = clinic_documents.basic_medical_record_id").select('surname,other_names,patients.mobile_number,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,basic_med_records.id,complaints,symptoms,vital_signs,clinic_exams,final_diagnosis,investigations,treatment_plans,card_no,clinic_documents.created_at,clinic_documents.patient_id,clinic_documents.id').where(patient_id: patient_id,status: 1 ).order('id desc')

      logger.info "CLINIC DOCUMENTS: #{@clinic_documents}"

      @document1 = ClinicDocument.find_by(id: patient_id)
      logger.info "document1: #{@document1}"
      logger.info "complaints: #{@document1.complaints}"

    else

      logger.info "XXXXXXXXXXXXXXXXXXXXXX"

      @clinic_documents = ClinicDocument.joins("INNER JOIN patients ON patients.id = clinic_documents.patient_id
                    INNER JOIN users ON users.id = clinic_documents.user_id").select('surname,other_names,patients.mobile_number,fullname,occupation,address,dob,patients.user_id,complaints,symptoms,vital_signs,clinic_exams,final_diagnosis,investigations,treatment_plans,card_no,clinic_documents.created_at,clinic_documents.patient_id,clinic_documents.id').where(patient_id: patient_id ,status: 1).order('id desc')

    end

    respond_to do |format|

      format.html
      format.js
    end

  end

  def assign_to_a_doctor
    logger.info "---------------------------ASSIGED DOCTOR---------------------------------"

    personnel_id = params[:assign_patient][:personnel_id]
    patient_id = params[:assign_patient][:patient_id]
    role_id = params[:assign_patient][:role_id]
    user_id = params[:assign_patient][:user_id]
    status = params[:assign_patient][:status]
    basic_medical_record_id = params[:assign_patient][:basic_medical_record_id]

    if personnel_id == '1'

      service_id = 4
      @services_data = ServicesInventory.find_by(id: service_id)
      price = @services_data.price
    
    if BillTemp.where(patient_id: patient_id).exists?
      @id = BillTemp.find_by(patient_id: patient_id)
      @bills = BillHistory.new({:service_id => service_id,:patient_id => patient_id, :price => price ,:status=> 0})
      @bills_temp = BillTemp.update(@id.id, :created_at => Time.now )
      @bills.save

    else
      @bills = BillHistory.new({:service_id => service_id,:patient_id => patient_id, :price => price ,:status=> 0})
      @bills_temp = BillTemp.new({:patient_id => patient_id, :status=> 0})
    @bills.save
    @bills_temp.save
    end
   end

    @assign_to_doctor = AssignPatient.new({:patient_id => patient_id,:role_id => role_id, :status => status ,:personnel_id=> personnel_id,:user_id=> user_id,:basic_medical_record_id=> basic_medical_record_id})

    respond_to do |format|
      if @assign_to_doctor.save

        format.html { redirect_to basic_med_records_path, notice: 'Patient was successfully assigned to a Doctor.' }
        format.json { render :show, status: :created, location: @assign_patient }

      else
        format.html { render :new }
        format.json { render json: @assign_patient.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /assign_patients
  # POST /assign_patients.json
  def create

    personnel_id = params[:assign_patient][:personnel_id]
    patient_id = params[:assign_patient][:patient_id]

    if personnel_id == '1'

      service_id = 4
      @services_data = ServicesInventory.find_by(id: service_id)
      price = @services_data.price

      if BillTemp.where(patient_id: patient_id).exists?
        @id = BillTemp.find_by(patient_id: patient_id)
        @bills = BillHistory.new({:service_id => service_id,:patient_id => patient_id, :price => price ,:status=> 0})
        @bills_temp = BillTemp.update(@id.id, :created_at => Time.now )
      @bills.save

      else
        @bills = BillHistory.new({:service_id => service_id,:patient_id => patient_id, :price => price ,:status=> 0})
        @bills_temp = BillTemp.new({:patient_id => patient_id, :status=> 0})
      @bills.save
      @bills_temp.save
      end

    elsif  personnel_id == '3'

      service_id = 5
      @services_data = ServicesInventory.find_by(id: service_id)
      price = @services_data.price

      if BillTemp.where(patient_id: patient_id).exists?
        @id = BillTemp.find_by(patient_id: patient_id)
        @bills = BillHistory.new({:service_id => service_id,:patient_id => patient_id, :price => price ,:status=> 0})
        @bills_temp = BillTemp.update(@id.id, :created_at => Time.now )
      @bills.save

      else
        @bills = BillHistory.new({:service_id => service_id,:patient_id => patient_id, :price => price ,:status=> 0})
        @bills_temp = BillTemp.new({:patient_id => patient_id, :status=> 0})
      @bills.save
      @bills_temp.save
      end

    else
      @bills_temp = BillTemp.new({:patient_id => patient_id, :status=> 0})
    @bills_temp.save

    end

    @assign_patient = AssignPatient.new(assign_patient_params)

    respond_to do |format|
      if @assign_patient.save
        if personnel_id == '1'
          # flash[:success] = "Assign patient was successfully created"
          format.html { redirect_to patients_path, notice: 'Patient was successfully assigned to a Doctor.' }
          format.json { render :show, status: :created, location: @assign_patient }

        elsif personnel_id == '2'
          # flash[:success] = "Assign patient was successfully created"
          format.html { redirect_to patients_path, notice: 'Patient was successfully assigned to a Lab technician.' }
          format.json { render :show, status: :created, location: @assign_patient }

        elsif personnel_id == '3'
          # flash[:success] = "Assign patient was successfully created"
          format.html { redirect_to patients_path, notice: 'Patient was successfully assigned to a nurse.' }
          format.json { render :show, status: :created, location: @assign_patient }

        elsif personnel_id == '5'
          # flash[:success] = "Assign patient was successfully created"
          format.html { redirect_to patients_path, notice: 'Patient was successfully assigned to a pharmacist.' }
          format.json { render :show, status: :created, location: @assign_patient }

        else

          format.html { redirect_to patients_path, notice: 'Patient was successfully nurse only.' }
          format.json { render :show, status: :created, location: @assign_patient }

        end
      else
        format.html { render :new }
        format.json { render json: @assign_patient.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /assign_patients/1
  # PATCH/PUT /assign_patients/1.json
  def update
    respond_to do |format|
      if @assign_patient.update(assign_patient_params)
        format.html { redirect_to @assign_patient, notice: 'Assign patient was successfully updated.' }
        format.json { render :show, status: :ok, location: @assign_patient }
      else
        format.html { render :edit }
        format.json { render json: @assign_patient.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assign_patients/1
  # DELETE /assign_patients/1.json
  def destroy
    @assign_patient.destroy
    respond_to do |format|
      format.html { redirect_to assign_patients_url, notice: 'Assign patient was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_assign_patient
    @assign_patient = AssignPatient.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def assign_patient_params
    params.require(:assign_patient).permit(:patient_id, :role_id, :status,:personnel_id,:user_id, :basic_medical_record_id)
  end
end

class BasicMedRecordsController < ApplicationController
  before_action :set_basic_med_record, only: [:show, :edit, :update, :destroy]

  layout "doc_patients",:only => [ :nurse_admission_vitals_history,:admission_basic_medical]
  # GET /basic_med_records
  # GET /basic_med_records.json
  def index
    @basic_med_records = BasicMedRecord.all
    # @assign_patient = AssignPatient.new
    @assign_to_doctor = AssignPatient.new

    @basic_med_records= BasicMedRecord.joins("INNER JOIN patients ON patients.id = basic_med_records.patient_id
                    INNER JOIN users ON users.id = basic_med_records.user_id").select('surname,other_names,patients.mobile_number,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,patient_id,basic_med_records.id,basic_med_records.created_at').where(user_id: current_user.id,status: 1 ).order('id desc')
  end

  def nurse_admission_vitals_history

    #@assign_patient = AssignPatient.new
    # @clinic_document = ClinicDocument.new
    @basic_med_record = BasicMedRecord.new

    patient_id =params[:id]
    @patient_id = patient_id

    @patient_records = Patient.find_by(id: patient_id)

    logger.info "Lets see patient_id id #{@patient_id}"
    logger.info "Lets see  Patient name #{@patient_records.surname } #{@patient_records.other_names }"

    @basic_med_records= BasicMedRecord.joins("INNER JOIN patients ON patients.id = basic_med_records.patient_id
                    INNER JOIN users ON users.id = basic_med_records.user_id").select('surname,other_names,patients.mobile_number,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,patient_id,basic_med_records.admission_status,basic_med_records.id,basic_med_records.created_at,card_no').where(patient_id: patient_id,admission_status: 1 ).order('id desc')

    respond_to do |format|

      format.html
      format.js
    end

  end

  def admission_basic_medical
    @assign_patient = AssignPatient.new
    @basic_med_record = BasicMedRecord.new
    patient_id=params[:id]

    logger.info "Lets see id para #{patient_id}"

    @records = Patient.find_by(id: patient_id)

    logger.info "Lets see the records"
    logger.info "record: #{@records.inspect}"

    @patient_id = @records.id
    patient_id = @patient_id
    logger.info "patient id: #{@patient_id}"
    p "record: #{@patients.inspect}"

    respond_to do |format|

      format.html
      format.js
    end
  end

  # G ET /basic_med_records/1
  # GET /basic_med_records/1.json
  def show
  end

  # GET /basic_med_records/new
  def new
    @basic_med_record = BasicMedRecord.new
  end

  # GET /basic_med_records/1/edit
  def edit
  end

  def assign_a_patient2
    @assign_to_doctor = AssignPatient.new
    @assign_patient = AssignPatient.new

    basic_med_id=params[:id]

    @b_records = BasicMedRecord.find_by(id: basic_med_id)

    logger.info "------------------------------------------"
    logger.info "Lets see the b records"
    logger.info "record: #{@b_records.inspect}"
    logger.info "------------------------------------------"

    @patient_id = @b_records.patient_id
    patient_id = @patient_id

    @basic_med_id = @b_records.id
    basic_med_id = @basic_med_id
    logger.info "patient id: #{@patient_id}"
    logger.info " basic_med_id: #{@basic_med_id}"

    respond_to do |format|

      format.html
      format.js
    end
  end

  def view_nurse_record

    created_at=params[:id]

    @documents = BasicMedRecord.where(created_at: created_at)
    logger.info "document1: #{@documents.inspect}"

    @vital_signs = "Respiratory Rate " + @documents[0].respiratory_rate + " " + "Pulse " +  @documents[0].pulse + " " + "blood pressure " +  @documents[0].blood_pressure + " " + "Temperature" +  @documents[0].temp

    logger.info "Vital signs: #{@vital_signs}"

    respond_to do |format|

      format.html
      format.js
    end

  end

  # POST /basic_med_records
  # POST /basic_med_records.json
  def create
    status = params[:basic_med_record][:status]
    patient_id = params[:basic_med_record][:patient_id]
    logger.info "STATUS OF BASC+IC MED RECORD #{status}"

    @basic_med_record = BasicMedRecord.new(basic_med_record_params)

    respond_to do |format|
      if @basic_med_record.save

        if status = 0
          format.html { redirect_to nurse_admission_vitals_history_path(:id => patient_id), notice: 'Admission Medical Record has been successfully created.' }
          format.json { render :show, status: :created, location: @basic_med_record }
        else
          format.html { redirect_to basic_med_records_path, notice: 'Basic Medical Record has been successfully created.' }
          format.json { render :show, status: :created, location: @basic_med_record }
        end
      else
        format.html { render :new }
        format.json { render json: @basic_med_record.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /basic_med_records/1
  # PATCH/PUT /basic_med_records/1.json
  def update
    respond_to do |format|
      if @basic_med_record.update(basic_med_record_params)
        format.html { redirect_to @basic_med_record, notice: 'Basic med record was successfully updated.' }
        format.json { render :show, status: :ok, location: @basic_med_record }
      else
        format.html { render :edit }
        format.json { render json: @basic_med_record.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /basic_med_records/1
  # DELETE /basic_med_records/1.json
  def destroy
    @basic_med_record.destroy
    respond_to do |format|
      format.html { redirect_to basic_med_records_url, notice: 'Basic med record was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_basic_med_record
    @basic_med_record = BasicMedRecord.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def basic_med_record_params
    params.require(:basic_med_record).permit(:patient_id, :blood_pressure, :pulse, :temp, :respiratory_rate, :user_id,:status,:complaints, :admission_status)
  end
end

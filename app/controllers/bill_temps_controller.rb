class BillTempsController < ApplicationController
  before_action :set_bill_temp, only: [:show, :edit, :update, :destroy]
  # GET /bill_temps
  # GET /bill_temps.json
  def index
    #@bill_temps = BillTemp.all

    @bill_temps = BillTemp.joins("INNER JOIN patients ON patients.id = bill_temps.patient_id
                    ").select('surname,other_names,bill_temps.patient_id,bill_temps.created_at').order('bill_temps.created_at desc')
  end

  def bill_open

    @assign_patient = AssignPatient.new
    patient_id=params[:id]

    # @bills = BillHistory.where(patient_id: patient_id)

    @bill_histories = BillHistory.joins("INNER JOIN patients ON patients.id = bill_histories.patient_id
                    INNER JOIN services_inventories ON services_inventories.id = bill_histories.service_id").select('bill_histories.id,surname,other_names,bill_histories.price,bill_histories.quantity,service_name,bill_histories.patient_id,bill_histories.created_at,bill_histories.status').where(patient_id: patient_id)

    @bill_total = BillHistory.where(patient_id: patient_id, status: 0)
    @total = @bill_total.sum(:price)

    logger.info "PRICE IS #{@total}"
    # logger.info "record: #{@record.inspect}"

    respond_to do |format|

      format.html
      format.js
    end
  end

  def outstanding
  
    @bill_history_id = params[:id]
    
    @bill_id = BillHistory.find_by(id: @bill_history_id)
    @bill_status = "true"
    BillHistory.update(@bill_history_id, :status => 1)
    
  end

  def paid
    bill_history_id =params[:id]
    if BillHistory.update(bill_history_id, :status => 0)
      redirect_to bill_temps_path, notice: 'Bills has not been paid'
    end
  end


  def print_all_bill

    patient_id=params[:id]

    @bill_histories = BillHistory.joins("INNER JOIN patients ON patients.id = bill_histories.patient_id
                    INNER JOIN services_inventories ON services_inventories.id = bill_histories.service_id").select('bill_histories.id,surname,other_names,bill_histories.price,bill_histories.quantity,service_name,bill_histories.patient_id,bill_histories.created_at,bill_histories.status').where(patient_id: patient_id)

    logger.info "BILL HISTOURES #{@bill_histories.inspect}"
    
    @bill_total = BillHistory.where(patient_id: patient_id, status: 1)
    @total = @bill_total.sum(:price)
     logger.info "PRICE IS #{@total}"

    respond_to do |format|

      format.html

      format.pdf do
        pdf = AllBills.new(@bill_histories)
        #pdf.text "HIIIII"
        send_data pdf.render, filename: "Totalbills.pdf",
                                type: "application/pdf",
                                dispostion: "inline"
      end
    end
  end






   def print_bill

    
    bill_id=params[:id]

    # @documents = BillHistory.find_by(id: @bill_history_id)
    @bill_histories = BillHistory.joins("INNER JOIN patients ON patients.id = bill_histories.patient_id
                    INNER JOIN services_inventories ON services_inventories.id = bill_histories.service_id").select('bill_histories.id,surname,other_names,bill_histories.price,bill_histories.quantity,service_name,bill_histories.patient_id,bill_histories.created_at,bill_histories.status').where(id: bill_id)

  logger.info "BILL HISTOURES #{@bill_histories.inspect}"
  
    respond_to do |format|

      format.html

      format.pdf do
        pdf = PatientBillPdf.new(@bill_histories)
        #pdf.text "HIIIII"
        send_data pdf.render, filename: "bills.pdf",
                                type: "application/pdf",
                                dispostion: "inline"
      end
    end
  end



  # GET /bill_temps/1
  # GET /bill_temps/1.json
  def show
  end

  # GET /bill_temps/new
  def new
    @bill_temp = BillTemp.new
  end

  # GET /bill_temps/1/edit
  def edit
  end

  # POST /bill_temps
  # POST /bill_temps.json
  def create
    @bill_temp = BillTemp.new(bill_temp_params)

    respond_to do |format|
      if @bill_temp.save
        format.html { redirect_to @bill_temp, notice: 'Bill temp was successfully created.' }
        format.json { render :show, status: :created, location: @bill_temp }
      else
        format.html { render :new }
        format.json { render json: @bill_temp.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bill_temps/1
  # PATCH/PUT /bill_temps/1.json
  def update
    respond_to do |format|
      if @bill_temp.update(bill_temp_params)
        format.html { redirect_to @bill_temp, notice: 'Bill temp was successfully updated.' }
        format.json { render :show, status: :ok, location: @bill_temp }
      else
        format.html { render :edit }
        format.json { render json: @bill_temp.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bill_temps/1
  # DELETE /bill_temps/1.json
  def destroy
    @bill_temp.destroy
    respond_to do |format|
      format.html { redirect_to bill_temps_url, notice: 'Bill temp was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_bill_temp
    @bill_temp = BillTemp.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def bill_temp_params
    params.require(:bill_temp).permit(:patient_id, :user_id, :status)
  end
end

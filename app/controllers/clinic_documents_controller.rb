class ClinicDocumentsController < ApplicationController
  before_action :set_clinic_document, only: [:show, :edit, :update, :destroy]

  # GET /clinic_documents
  # GET /clinic_documents.json
  def index
    @clinic_documents = ClinicDocument.joins("INNER JOIN patients ON patients.id = clinic_documents.patient_id
                    INNER JOIN users ON users.id = clinic_documents.user_id INNER JOIN basic_med_records ON basic_med_records.id = clinic_documents.basic_medical_record_id").select('surname,other_names,patients.mobile_number,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,basic_med_records.id,complaints,symptoms,vital_signs,clinic_exams,final_diagnosis,investigations,treatment_plans').where(user_id: current_user.id,status: 1 ).order('id desc')

  end

  # GET /clinic_documents/1
  # GET /clinic_documents/1.json
  def show
  end

  # GET /clinic_documents/new
  def new
    @clinic_document = ClinicDocument.new
  end

  

  # GET /clinic_documents/1/edit
  def edit
  end

  # POST /clinic_documents
  # POST /clinic_documents.json
  def create

  patient_id =  params[:clinic_document][:patient_id] 
  status = params[:clinic_document][:status]
    
    @clinic_document = ClinicDocument.new(clinic_document_params)

    respond_to do |format|
      if @clinic_document.save
        if status == true
        format.html { redirect_to docs_path(:id => patient_id), notice: 'Clinic document was successfully created.' }
        format.json { render :show, status: :created, location: @clinic_document }
      else
         format.html { redirect_to admission_vitals_path(:id => patient_id), notice: 'Admission document was successfully created.' }
        format.json { render :show, status: :created, location: @clinic_document }
      end
      else
        format.html { render :new }
        format.json { render json: @clinic_document.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /clinic_documents/1
  # PATCH/PUT /clinic_documents/1.json
  def update
    respond_to do |format|
      if @clinic_document.update(clinic_document_params)
        format.html { redirect_to @clinic_document, notice: 'Clinic document was successfully updated.' }
        format.json { render :show, status: :ok, location: @clinic_document }
      else
        format.html { render :edit }
        format.json { render json: @clinic_document.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /clinic_documents/1
  # DELETE /clinic_documents/1.json
  def destroy
    @clinic_document.destroy
    respond_to do |format|
      format.html { redirect_to clinic_documents_url, notice: 'Clinic document was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_clinic_document
      @clinic_document = ClinicDocument.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def clinic_document_params
      params.require(:clinic_document).permit(:patient_id, :basic_medical_record_id, :user_id, :status, :complaints, :symptoms, :vital_signs, :clinic_exams, :final_diagnosis, :investigations, :treatment_plans)
    end
end

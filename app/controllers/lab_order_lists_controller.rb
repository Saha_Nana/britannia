class LabOrderListsController < ApplicationController
  before_action :set_lab_order_list, only: [:show, :edit, :update, :destroy]

  # GET /lab_order_lists
  # GET /lab_order_lists.json
  def index
    @lab_order_lists = LabOrderList.all
    @lab_order_list = LabOrderList.new
  end

  # GET /lab_order_lists/1
  # GET /lab_order_lists/1.json
  def show
  end

  # GET /lab_order_lists/new
  def new
    @lab_order_list = LabOrderList.new
  end
  
 

  # GET /lab_order_lists/1/edit
  def edit
  end

  # POST /lab_order_lists
  # POST /lab_order_lists.json
  def create
    @lab_order_list = LabOrderList.new(lab_order_list_params)

    respond_to do |format|
      if @lab_order_list.save
        format.html { redirect_to @lab_order_list, notice: 'Lab order list was successfully created.' }
        format.json { render :show, status: :created, location: @lab_order_list }
      else
        format.html { render :new }
        format.json { render json: @lab_order_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /lab_order_lists/1
  # PATCH/PUT /lab_order_lists/1.json
  def update
    respond_to do |format|
      if @lab_order_list.update(lab_order_list_params)
        format.html { redirect_to @lab_order_list, notice: 'Lab order list was successfully updated.' }
        format.json { render :show, status: :ok, location: @lab_order_list }
      else
        format.html { render :edit }
        format.json { render json: @lab_order_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lab_order_lists/1
  # DELETE /lab_order_lists/1.json
  def destroy
    @lab_order_list.destroy
    respond_to do |format|
      format.html { redirect_to lab_order_lists_url, notice: 'Lab order list was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lab_order_list
      @lab_order_list = LabOrderList.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def lab_order_list_params
      params.require(:lab_order_list).permit(:lab_name, :price, :status)
    end
end

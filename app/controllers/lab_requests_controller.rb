class LabRequestsController < ApplicationController
  before_action :set_lab_request, only: [:show, :edit, :update, :destroy]
  layout "doc_patients",:only => [ :lab_docs]

  autocomplete :services_inventories, :service_name ,:full => true
  # GET /lab_requests
  # GET /lab_requests.json
  def index

    @lab_requests = LabRequest.joins("INNER JOIN patients ON patients.id = lab_requests.patient_id
                    INNER JOIN users ON users.id = lab_requests.user_id INNER JOIN basic_med_records ON basic_med_records.id = lab_requests.basic_medical_record_id").select('surname,other_names,patients.mobile_number,card_no,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,basic_med_records.id,lab_type,personnel_id').where(personnel_id: current_user.role_id )
    @assign_patients= AssignPatient.joins("INNER JOIN patients ON patients.id = assign_patients.patient_id
                INNER JOIN users ON users.id = assign_patients.user_id").select('surname,other_names,patients.mobile_number,fullname,occupation,address,dob,patients.user_id,patient_id,assign_patients.id,assign_patients.status,basic_medical_record_id,personnel_id,users.role_id,card_no').where(personnel_id: current_user.role_id).order('id desc')

  end

  # GET /lab_requests/1
  # GET /lab_requests/1.json
  def show
  end

  # GET /lab_requests/new
  def new
    @lab_request = LabRequest.new
  end

  # GET /lab_requests/1/edit
  def edit
  end

  # POST /lab_requests
  # POST /lab_requests.json
  def create

    service_id = params[:lab_request][:service_id]
    patient_id = params[:lab_request][:patient_id]
    @services_data = ServicesInventory.find_by(id: service_id)
    price = @services_data.price
    logger.info "--------------------------------------------"
    logger.info "-------------service_id#{service_id}-------------------------------"
    logger.info "-------------patient_id#{patient_id}-------------------------------"
    logger.info "-------------price#{price}-------------------------------"
    logger.info "--------------------------------------------"
    
    
     if BillTemp.where(patient_id: patient_id).exists?
      @id = BillTemp.find_by(patient_id: patient_id)
      @bills = BillHistory.new({:service_id => service_id,:patient_id => patient_id, :price => price ,:status=> 0})
      @bills_temp = BillTemp.update(@id.id, :created_at => Time.now )
      @bills.save

    else
      @bills = BillHistory.new({:service_id => service_id,:patient_id => patient_id, :price => price ,:status=> 0})
      @bills_temp = BillTemp.new({:patient_id => patient_id, :status=> 0})
    @bills.save
    @bills_temp.save
    end

    @lab_request = LabRequest.new(lab_request_params)
    assign_patients_path
    respond_to do |format|
      if @lab_request.save
        format.html { redirect_to lab_docs_path(:id => patient_id), notice: 'Lab request was successfully created.' }
        format.json { render :show, status: :created, location: @lab_request }
      else
        format.html { render :new }
        format.json { render json: @lab_request.errors, status: :unprocessable_entity }
      end
    end
  end

  def lab_docs

    @assign_patient = AssignPatient.new
    @clinic_document = ClinicDocument.new
    @lab_request = LabRequest.new

    patient_id =params[:id]
    @patient_id = patient_id

    @patient_records = Patient.find_by(id: patient_id)

    logger.info "Lets see patient_id id #{@patient_id}"
    logger.info "Lets see  Patient name #{@patient_records.surname } #{@patient_records.other_names }"
    if (defined?(patient_id)).nil?

      @lab_requests = LabRequest.joins("INNER JOIN patients ON patients.id = lab_requests.patient_id
                    INNER JOIN users ON users.id = lab_requests.user_id INNER JOIN basic_med_records ON basic_med_records.id = lab_requests.basic_medical_record_id").select('surname,other_names,patients.mobile_number,card_no,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,basic_med_records.id,lab_type,lab_requests.created_at,lab_requests.patient_id,personnel_id').where(patient_id: patient_id )

      @b_records = BasicMedRecord.find_by(id: patient_id)
      @blood_pressure = @b_records.blood_pressure
      @pulse = @b_records.pulse
      @respiratory = @b_records.respiratory_rate
      @temperature = @b_records.temp
      @patient_id = @b_records.patient_id

      patient_id = @patient_id

      @basic_med_id = @b_records.id
      basic_med_id = @basic_med_id

      logger.info "Lets see medical id #{basic_med_id}"

      @clinic_documents = ClinicDocument.joins("INNER JOIN patients ON patients.id = clinic_documents.patient_id
                    INNER JOIN users ON users.id = clinic_documents.user_id INNER JOIN basic_med_records ON basic_med_records.id = clinic_documents.basic_medical_record_id").select('surname,other_names,patients.mobile_number,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,basic_med_records.id,complaints,symptoms,vital_signs,clinic_exams,final_diagnosis,investigations,treatment_plans,card_no,clinic_documents.created_at,clinic_documents.patient_id').where(patient_id: patient_id )

      logger.info "CLINIC DOCUMENTS: #{@clinic_documents}"

      @document1 = ClinicDocument.find_by(id: basic_med_id)
      logger.info "document1: #{@document1}"
      logger.info "complaints: #{@document1.complaints}"

    else

      @lab_requests = LabRequest.joins("INNER JOIN patients ON patients.id = lab_requests.patient_id
                    INNER JOIN users ON users.id = lab_requests.user_id").select('surname,other_names,patients.mobile_number,card_no,fullname,occupation,address,dob,patients.user_id,lab_type,lab_requests.created_at,lab_requests.patient_id,personnel_id').where(patient_id: patient_id )
    end

    respond_to do |format|

      format.html
      format.js
    end

  end

  def lab_form
    @assign_patient = AssignPatient.new
    @lab_request = LabRequest.new

    patient_id=params[:id]
    @patient_id = patient_id

    logger.info "Lets see medical id #{@patient_id}"

    @services_inventories = ServicesInventory.order(:service_name).where("service_name like ? AND category_id = 1", "%#{params[:service_id]}" )
    @service_list = @services_inventories.map { |a|[a.service_name+" ",a.id]  }

    @b_records = BasicMedRecord.find_by(patient_id: patient_id)

    if (defined?(@b_records)).nil?

      @patient_id = @b_records.patient_id
      patient_id = @patient_id

      @basic_med_id = @b_records.id
    basic_med_id = @basic_med_id

    else
    end

    respond_to do |format|

      format.html
      format.js
    end

  end

  def view_lab

    @assign_patient = AssignPatient.new
    #@clinic_document = ClinicDocument.new
    patient_id=params[:id]

    @b_records = BasicMedRecord.find_by(patient_id: patient_id)
    #   @blood_pressure = @b_records.blood_pressure
    # @pulse = @b_records.pulse
    # @respiratory = @b_records.respiratory_rate
    # @temperature = @b_records.temp
    # @patient_id = @b_records.patient_id
    # patient_id = @patient_id

    #  @basic_med_id = @b_records.id
    # basic_med_id = @basic_med_id

    # @document1 = LabRequest.find_by(patient_id: patient_id)

    @document1 = LabRequest.joins("INNER JOIN patients ON patients.id = lab_requests.patient_id
                    INNER JOIN services_inventories ON services_inventories.id = lab_requests.service_id").select('service_name').where(patient_id: patient_id )

    respond_to do |format|

      format.html
      format.js
    end

  end

  # PATCH/PUT /lab_requests/1
  # PATCH/PUT /lab_requests/1.json
  def update
    respond_to do |format|
      if @lab_request.update(lab_request_params)
        format.html { redirect_to @lab_request, notice: 'Lab request was successfully updated.' }
        format.json { render :show, status: :ok, location: @lab_request }
      else
        format.html { render :edit }
        format.json { render json: @lab_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lab_requests/1
  # DELETE /lab_requests/1.json
  def destroy
    @lab_request.destroy
    respond_to do |format|
      format.html { redirect_to lab_requests_url, notice: 'Lab request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_lab_request
    @lab_request = LabRequest.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def lab_request_params
    params.require(:lab_request).permit(:basic_medical_record_id, :patient_id, :user_id, :personnel_id, :status, :lab_type,:service_id)
  end
end

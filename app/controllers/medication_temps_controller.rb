class MedicationTempsController < ApplicationController
  before_action :set_medication_temp, only: [:show, :edit, :update, :destroy]

  # GET /medication_temps
  # GET /medication_temps.json
  def index
    
    
    @medication_temps = MedicationTemp.joins("INNER JOIN patients ON patients.id = medication_temps.patient_id
                    ").select('surname,other_names,medication_temps.patient_id,medication_temps.created_at,medication_temps.status').order('medication_temps.created_at desc')
  end
  
  
  def open_med_temp
     @services_inventories = ServicesInventory.new
      @patient_id=params[:id]

    # @medication_requests = OrderRequest.joins("INNER JOIN patients ON patients.id = order_requests.patient_id
                 #   INNER JOIN users ON users.id = order_requests.user_id INNER JOIN services_inventories ON services_inventories.id = order_requests.service_id").select('order_requests.id,services_inventories.service_name,services_inventories.category_id,surname,other_names,patients.mobile_number,card_no,fullname,occupation,address,dob,patients.user_id,order_requests.created_at,order_requests.patient_id,personnel_id,order_requests.service_id,order_requests.status,order_requests.id').where( 'services_inventories.category_id = 2 and patient_id = @patient_id').order('order_requests.id desc')
      
 @medication_requests = OrderRequest.joins("INNER JOIN patients ON patients.id = order_requests.patient_id
                    INNER JOIN users ON users.id = order_requests.user_id INNER JOIN services_inventories ON services_inventories.id = order_requests.service_id").select('order_requests.id,services_inventories.service_name,services_inventories.category_id,surname,other_names,patients.mobile_number,card_no,fullname,occupation,address,dob,patients.user_id,order_requests.created_at,order_requests.patient_id,personnel_id,order_requests.service_id,order_requests.status,order_requests.id').where( "services_inventories.category_id = ? AND patient_id = ?", 2, params[:id]).order('order_requests.id desc')
      




    respond_to do |format|

      format.html
      format.js
    end
  end
  
 def close_med_request
   
    @medication_request_id = params[:id]
    
    @order_id = OrderRequest.find_by(id: @medication_request_id)
    @order_status = "true"
    OrderRequest.update(@medication_request_id, :status => 1)
 
  end

  # GET /medication_temps/1
  # GET /medication_temps/1.json
  def show
  end

  # GET /medication_temps/new
  def new
    @medication_temp = MedicationTemp.new
  end

  # GET /medication_temps/1/edit
  def edit
  end

  # POST /medication_temps
  # POST /medication_temps.json
  def create
    @medication_temp = MedicationTemp.new(medication_temp_params)

    respond_to do |format|
      if @medication_temp.save
        format.html { redirect_to @medication_temp, notice: 'Medication temp was successfully created.' }
        format.json { render :show, status: :created, location: @medication_temp }
      else
        format.html { render :new }
        format.json { render json: @medication_temp.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /medication_temps/1
  # PATCH/PUT /medication_temps/1.json
  def update
    respond_to do |format|
      if @medication_temp.update(medication_temp_params)
        format.html { redirect_to @medication_temp, notice: 'Medication temp was successfully updated.' }
        format.json { render :show, status: :ok, location: @medication_temp }
      else
        format.html { render :edit }
        format.json { render json: @medication_temp.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /medication_temps/1
  # DELETE /medication_temps/1.json
  def destroy
    @medication_temp.destroy
    respond_to do |format|
      format.html { redirect_to medication_temps_url, notice: 'Medication temp was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_medication_temp
      @medication_temp = MedicationTemp.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def medication_temp_params
      params.require(:medication_temp).permit(:patient_id, :service_id, :status)
    end
end

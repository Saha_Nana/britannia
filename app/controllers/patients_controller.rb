class PatientsController < ApplicationController
  before_action :set_patient, only: [:show, :edit, :update, :destroy]
  respond_to :html, :js
  # GET /patients
  # GET /patients.json
  def index
    @patient = Patient.new
    @assign_patient = AssignPatient.new
    @reception_appointment = Appointment.new
    @patients = Patient.all.order('id desc')

    logger.info "-------------------------------------"
    logger.info "Lets see the patients"
    logger.info "record: #{@patients.inspect}"

 
  end

  def patient_number
    time=Time.new
    strtm = time.strftime("BR"+"%y%m%d%L%H%M")
    return strtm
  end

  # GET /patients/1
  # GET /patients/1.json
  def show
  end

  # GET /patients/new
  def new

    @patient = Patient.new

    patient_id=params[:id]

    @patients1 = Patient.find_by(id: patient_id)
    logger.info "patients1: #{@patients1}"
    @patients1.other_names
    logger.info "other_names: #{@patients1.other_names}"

  end

  ########### PARTIALS ###############

  def patients_form
    @patient_number = patient_number
    logger.info "Patient number is #{@patient_number}"
    

    @patient = Patient.new

    respond_to do |format|

      format.html
      format.js
    end
  end

  def show_patients
    patient_id=params[:id]

    @patients1 = Patient.find_by(id: patient_id)
    logger.info "patients1: #{@patients1}"
    @patients1.other_names
    logger.info "other_names: #{@patients1.other_names}"
  end

  def edit_patients
    patient_id=params[:id]
    @patient = Patient.new

    @patients1 = Patient.find_by(id: patient_id)
    logger.info "patients1: #{@patients1}"
    @patients1.other_names
    logger.info "other_names: #{@patients1.other_names}"
    logger.info "ID: #{@patients1.id}"

  end

  def assign_a_patient
    @assign_patient = AssignPatient.new
    patient_id=params[:id]

    logger.info "Lets see id para #{patient_id}"

    @patient = Patient.new
    @records = Patient.find_by(id: patient_id)

    logger.info "Lets see the records"
    logger.info "record: #{@records.inspect}"

    @patient_id = @records.id
    patient_id = @patient_id
    logger.info "patient id: #{@patient_id}"
    p "record: #{@patients.inspect}"

    respond_to do |format|

      format.html
      format.js
    end

  end

  def reception_patient
    @reception_appointment = Appointment.new
    @patient_id=params[:id]

    logger.info "Lets see id of patient in Appointment #{@patient_id}"

    respond_to do |format|

      format.html
      format.js
    end

  end
 

  # GET /patients/1/edit
  def edit
    patient_id=params[:id]

    @patient = Patient.find(params[:id])
  end

  # POST /patients
  # POST /patients.json
  def create
    @patient = Patient.new(patient_params)

    respond_to do |format|
      if @patient.save
        format.html { redirect_to patients_path, notice: 'Patient Record was successfully created.' }
        format.json { render :show, status: :created, location: @patient }
      else
        format.html { render :new }
        #format.html { redirect_to patients_form_path, notice: 'Check fields.' }
        format.json { render json: @patient.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /patients/1
  # PATCH/PUT /patients/1.json
  def update
    id=params[:id]
    respond_to do |format|
      id=params[:id]
      if @patient.update(patient_params)
        # format.html { redirect_to @patient, notice: 'Patient was successfully updated.' }
        format.html { redirect_to patients_path, notice: 'Patient was successfully updated.' }
        format.json { render :show, status: :ok, location: @patient }
      else
        format.html { render :edit }
        format.json { render json: @patients1.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /patients/1
  # DELETE /patients/1.json
  def destroy
    @patient.destroy
    respond_to do |format|
      format.html { redirect_to patients_url, notice: 'Patient was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_patient
    @patient = Patient.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def patient_params
    params.require(:patient).permit(:id,:title, :surname, :other_names, :mobile_number, :occupation, :address, :status, :user_id, :alt_number, :dob,:card_no)
  end
end

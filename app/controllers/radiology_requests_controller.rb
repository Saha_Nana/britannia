class RadiologyRequestsController < ApplicationController
  before_action :set_radiology_request, only: [:show, :edit, :update, :destroy]
  layout "doc_patients",:only => [ :radio_docs]

  # GET /radiology_requests
  # GET /radiology_requests.json
  def index
    

     @radiology_requests = RadiologyRequest.joins("INNER JOIN patients ON patients.id = radiology_requests.patient_id
                    INNER JOIN users ON users.id = radiology_requests.user_id INNER JOIN basic_med_records ON basic_med_records.id = radiology_requests.basic_medical_record_id").select('surname,other_names,patients.mobile_number,card_no,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,basic_med_records.id,radiology').where(personnel_id: current_user.role_id )

      @assign_patients= AssignPatient.joins("INNER JOIN patients ON patients.id = assign_patients.patient_id
                  INNER JOIN users ON users.id = assign_patients.user_id").select('surname,other_names,patients.mobile_number,fullname,occupation,address,dob,patients.user_id,patient_id,assign_patients.id,assign_patients.status,basic_medical_record_id,personnel_id,users.role_id,card_no').where(personnel_id: current_user.role_id).order('id desc')
  end


   def other_radio
    
      @radiology_requests = OrderRequest.joins("INNER JOIN patients ON patients.id = order_requests.patient_id
                    INNER JOIN users ON users.id = order_requests.user_id INNER JOIN services_inventories ON services_inventories.id = order_requests.service_id").select('services_inventories.service_name,services_inventories.category_id,surname,other_names,patients.mobile_number,card_no,fullname,occupation,address,dob,patients.user_id,order_requests.created_at,order_requests.patient_id,personnel_id,order_requests.service_id,order_requests.status,order_requests.id').where('services_inventories.category_id = 3').order('order_requests.id desc')
      
  end

    def close_request_rad
    med_id =params[:id]
    logger.info "Lets see medical id #{med_id}"
    if OrderRequest.update(med_id, :status => 1)
      redirect_to other_radio_path, notice: 'Request Closed.'
    end
  end

  def open_request_rad
    med_id =params[:id]
    if OrderRequest.update(med_id, :status => 0)
      redirect_to other_radio_path, notice: 'Request Open' 
    end
  end

  # GET /radiology_requests/1
  # GET /radiology_requests/1.json
  def show
  end

  # GET /radiology_requests/new
  def new
    @radiology_request = RadiologyRequest.new
  end

  # GET /radiology_requests/1/edit
  def edit
  end


    def radio_docs

     @assign_patient = AssignPatient.new
      @clinic_document = ClinicDocument.new
      @radiology_request = RadiologyRequest.new

   
      patient_id =params[:id] 
   @patient_id = patient_id

    @patient_records = Patient.find_by(id: patient_id)
     

    logger.info "Lets see patient_id id #{@patient_id}" 
    logger.info "Lets see  Patient name #{@patient_records.surname } #{@patient_records.other_names }" 

        if (defined?(patient_id)).nil? 

     @radiology_requests = RadiologyRequest.joins("INNER JOIN patients ON patients.id = radiology_requests.patient_id
                    INNER JOIN users ON users.id = radiology_requests.user_id INNER JOIN basic_med_records ON basic_med_records.id = radiology_requests.basic_medical_record_id").select('surname,other_names,patients.mobile_number,card_no,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,basic_med_records.id,radiology,card_no,radiology_requests.created_at,radiology_requests.patient_id').where(patient_id: patient_id )
    

      @b_records = BasicMedRecord.find_by(patient_id: patient_id)
       @blood_pressure = @b_records.blood_pressure
     @pulse = @b_records.pulse
     @respiratory = @b_records.respiratory_rate
     @temperature = @b_records.temp
     @patient_id = @b_records.patient_id
  
     patient_id = @patient_id

      @basic_med_id = @b_records.id
     basic_med_id = @basic_med_id
   
    

    @clinic_documents = ClinicDocument.joins("INNER JOIN patients ON patients.id = clinic_documents.patient_id
                    INNER JOIN users ON users.id = clinic_documents.user_id INNER JOIN basic_med_records ON basic_med_records.id = clinic_documents.basic_medical_record_id").select('surname,other_names,patients.mobile_number,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,basic_med_records.id,complaints,symptoms,vital_signs,clinic_exams,final_diagnosis,investigations,treatment_plans,card_no,clinic_documents.created_at,clinic_documents.patient_id').where(patient_id: patient_id )
   

     logger.info "CLINIC DOCUMENTS: #{@clinic_documents}"
  
     @document1 = ClinicDocument.find_by(patient_id: patient_id)
      logger.info "document1: #{@document1}"
       logger.info "complaints: #{@document1.complaints}"


     else
       @radiology_requests = RadiologyRequest.joins("INNER JOIN patients ON patients.id = radiology_requests.patient_id
                    INNER JOIN users ON users.id = radiology_requests.user_id").select('surname,other_names,patients.mobile_number,card_no,fullname,occupation,address,dob,patients.user_id,radiology,card_no,radiology_requests.created_at,radiology_requests.patient_id').where(patient_id: patient_id )

     end

    

    respond_to do |format|

        format.html
        format.js
  end

  end



    def radio_form

 

   @assign_patient = AssignPatient.new
 @radiology_request = RadiologyRequest.new

     patient_id=params[:id] 
     @patient_id = patient_id
   
    logger.info "Lets see medical id #{@patient_id}"


   

 @b_records = BasicMedRecord.find_by(patient_id: patient_id)

      if (defined?(@b_records)).nil? 

    @patient_id = @b_records.patient_id
     patient_id = @patient_id
     
      @basic_med_id = @b_records.id
     basic_med_id = @basic_med_id

   else
   end

    respond_to do |format|

        format.html
        format.js
  end
       
  end

  def view_radio

     @assign_patient = AssignPatient.new
    #@clinic_document = ClinicDocument.new
    patient_id=params[:id] 

      @b_records = BasicMedRecord.find_by(patient_id: patient_id)
    #    @blood_pressure = @b_records.blood_pressure
    #  @pulse = @b_records.pulse
    #  @respiratory = @b_records.respiratory_rate
    #  @temperature = @b_records.temp
    #  @patient_id = @b_records.patient_id
    #  patient_id = @patient_id

    #   @basic_med_id = @b_records.id
    #  basic_med_id = @basic_med_id
   
    # logger.info "Lets see medical id #{basic_med_id}"

    
   
    
     @document1 = RadiologyRequest.find_by(patient_id: patient_id)
      

    

    respond_to do |format|

        format.html
        format.js
  end

  end





   def other_lab
 @test_result = TestResult.new

    @lab_requests = LabRequest.joins("INNER JOIN patients ON patients.id = lab_requests.patient_id
                    INNER JOIN users ON users.id = lab_requests.user_id
                    INNER JOIN services_inventories ON services_inventories.id = lab_requests.service_id").select('service_id,service_name,patients.surname,patients.other_names,patients.mobile_number,patients.card_no,fullname,patients.occupation,patients.address,patients.dob,patients.user_id,lab_type,lab_requests.created_at,lab_requests.patient_id,personnel_id,lab_requests.id').order('id desc')

    @service_id =  @lab_requests[0].service_id
    logger.info "SERVICE ID #{@service_id}"

  end


     def lab_history

     @assign_patient = AssignPatient.new
  
    patient_id =params[:id] 

  
    
     @lab_history = LabRequest.joins("INNER JOIN patients ON patients.id = lab_requests.patient_id
                    INNER JOIN users ON users.id = lab_requests.user_id INNER JOIN basic_med_records ON basic_med_records.id = lab_requests.basic_medical_record_id").select('surname,other_names,patients.mobile_number,card_no,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,basic_med_records.id,lab_type,lab_requests.created_at,lab_requests.patient_id,personnel_id,lab_type').where(patient_id: patient_id)

    
    respond_to do |format|

        format.html
        format.js
  end

  end


def enter_results

  @test_result = TestResult.new
  lab_id=params[:id] 
  @lab_id = lab_id


    @lab_requests = LabRequest.joins("INNER JOIN patients ON patients.id = lab_requests.patient_id
                    INNER JOIN users ON users.id = lab_requests.user_id
                    INNER JOIN services_inventories ON services_inventories.id = lab_requests.service_id").select('service_id,service_name,patients.surname,patients.other_names,patients.mobile_number,patients.card_no,fullname,patients.occupation,patients.address,patients.dob,patients.user_id,lab_type,lab_requests.created_at,lab_requests.patient_id,personnel_id,lab_requests.id').where(personnel_id: current_user.role_id).order('id desc')

    @service_id =  @lab_requests[0].service_id
     @patient_id = @lab_requests[0].patient_id
    logger.info "SERVICE ID #{@service_id}"


  # @lab_details= LabRequest.find_by(id: lab_id)
  # @patient_id = @lab_details.patient_id
  logger.info "Lab ID : #{@lab_id}"
  logger.info "Patient ID : #{@patient_id}"
end


  # POST /radiology_requests
  # POST /radiology_requests.json
  def create
    @radiology_request = RadiologyRequest.new(radiology_request_params)

    respond_to do |format|
      if @radiology_request.save
        format.html { redirect_to radiology_requests_path, notice: 'Radiology request was successfully created.' }
        format.json { render :show, status: :created, location: @radiology_request }
      else
        format.html { render :new }
        format.json { render json: @radiology_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /radiology_requests/1
  # PATCH/PUT /radiology_requests/1.json
  def update
    respond_to do |format|
      if @radiology_request.update(radiology_request_params)
        format.html { redirect_to radiology_requests_path, notice: 'Radiology request was successfully updated.' }
        format.json { render :show, status: :ok, location: @radiology_request }
      else
        format.html { render :edit }
        format.json { render json: @radiology_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /radiology_requests/1
  # DELETE /radiology_requests/1.json
  def destroy
    @radiology_request.destroy
    respond_to do |format|
      format.html { redirect_to radiology_requests_url, notice: 'Radiology request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_radiology_request
      @radiology_request = RadiologyRequest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def radiology_request_params
      params.require(:radiology_request).permit(:basic_medical_record_id, :patient_id, :user_id, :personnel_id, :status, :radiology)
    end
end

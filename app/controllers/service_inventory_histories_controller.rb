class ServiceInventoryHistoriesController < ApplicationController
  before_action :set_service_inventory_history, only: [:show, :edit, :update, :destroy]

  # GET /service_inventory_histories
  # GET /service_inventory_histories.json
  def index
    @service_inventory_histories = ServiceInventoryHistory.all
  end

  # GET /service_inventory_histories/1
  # GET /service_inventory_histories/1.json
  def show
  end

  # GET /service_inventory_histories/new
  def new
    @service_inventory_history = ServiceInventoryHistory.new
  end

  # GET /service_inventory_histories/1/edit
  def edit
  end

  # POST /service_inventory_histories
  # POST /service_inventory_histories.json
  def create
    @service_inventory_history = ServiceInventoryHistory.new(service_inventory_history_params)

    respond_to do |format|
      if @service_inventory_history.save
        format.html { redirect_to @service_inventory_history, notice: 'Service inventory history was successfully created.' }
        format.json { render :show, status: :created, location: @service_inventory_history }
      else
        format.html { render :new }
        format.json { render json: @service_inventory_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /service_inventory_histories/1
  # PATCH/PUT /service_inventory_histories/1.json
  def update
    respond_to do |format|
      if @service_inventory_history.update(service_inventory_history_params)
        format.html { redirect_to @service_inventory_history, notice: 'Service inventory history was successfully updated.' }
        format.json { render :show, status: :ok, location: @service_inventory_history }
      else
        format.html { render :edit }
        format.json { render json: @service_inventory_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /service_inventory_histories/1
  # DELETE /service_inventory_histories/1.json
  def destroy
    @service_inventory_history.destroy
    respond_to do |format|
      format.html { redirect_to service_inventory_histories_url, notice: 'Service inventory history was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service_inventory_history
      @service_inventory_history = ServiceInventoryHistory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def service_inventory_history_params
      params.require(:service_inventory_history).permit(:service_id, :price)
    end
end

class ServicesInventoriesController < ApplicationController
  before_action :set_services_inventory, only: [:show, :edit, :update, :destroy]

  # GET /services_inventories
  # GET /services_inventories.json
  def index
    @services_inventories = ServicesInventory.all
    @services_inventory = ServicesInventory.new
  end

  # GET /services_inventories/1
  # GET /services_inventories/1.json
  def show
  end

  # GET /services_inventories/new
  def new
    @services_inventory = ServicesInventory.new
  end
  
  def import_csv_file
      @services_inventory = ServicesInventory.new
    
      respond_to do |format|
       format.html 
        format.js 
     end
 end
   
   
  def import
   @services_inventory = ServicesInventory.new
   
      if params[:file].nil?
        redirect_to services_inventories_path, notice: "Kindly select A file to upload"
      else               
        # check = ""        
        check = ServicesInventory.import_data(params[:file],"1")        
        logger.info check.inspect
        redirect_to services_inventories_path, notice: "Data successfully imported."
      end   
  end

  # GET /services_inventories/1/edit
  def edit
  end

  # POST /services_inventories
  # POST /services_inventories.json
  def create
    @services_inventory = ServicesInventory.new(services_inventory_params)

    respond_to do |format|
      if @services_inventory.save
        format.html { redirect_to services_inventories_path, notice: 'Services inventory was successfully created.' }
        format.json { render :show, status: :created, location: @services_inventory }
      else
        format.html { render :new }
        format.json { render json: @services_inventory.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /services_inventories/1
  # PATCH/PUT /services_inventories/1.json
  def update
    respond_to do |format|
      if @services_inventory.update(services_inventory_params)
        format.html { redirect_to services_inventories_path, notice: 'Services inventory was successfully updated.' }
        format.json { render :show, status: :ok, location: @services_inventory }
      else
        format.html { render :edit }
        format.json { render json: @services_inventory.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /services_inventories/1
  # DELETE /services_inventories/1.json
  def destroy
    @services_inventory.destroy
    respond_to do |format|
      format.html { redirect_to services_inventories_url, notice: 'Services inventory was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_services_inventory
      @services_inventory = ServicesInventory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def services_inventory_params
      params.require(:services_inventory).permit(:service_name, :price, :category_id, :quantifiable)
    end
end

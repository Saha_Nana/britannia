class TestResultsController < ApplicationController
  before_action :set_test_result, only: [:show, :edit, :update, :destroy]
  layout "doc_patients",:only => [ :main_test]
  # GET /test_results
  # GET /test_results.json
  def index
    # @test_results = TestResult.all

    @test_results = TestResult.joins("INNER JOIN patients ON patients.id = test_results.patient_id
                    INNER JOIN users ON users.id = test_results.user_id INNER JOIN lab_requests ON lab_requests.id = test_results.lab_id INNER JOIN services_inventories ON services_inventories.id = test_results.service_id").select('service_name,surname,other_names,patients.mobile_number,card_no,fullname,occupation,address,dob,patients.user_id,lab_type,results,test_results.patient_id,test_results.created_at,test_results.lab_id,test_results.id').order('id desc')

  end

  def main_test
    patient_id =params[:id]
    @patient_id = patient_id

    @patient_records = Patient.find_by(id: patient_id)

    @test_results = TestResult.joins("INNER JOIN patients ON patients.id = test_results.patient_id
                    INNER JOIN users ON users.id = test_results.user_id INNER JOIN lab_requests ON lab_requests.id = test_results.lab_id INNER JOIN services_inventories ON services_inventories.id = test_results.service_id").select('service_name,surname,other_names,patients.mobile_number,card_no,fullname,occupation,address,dob,patients.user_id,lab_type,results,test_results.patient_id,test_results.created_at,test_results.lab_id,test_results.id').where(patient_id: patient_id ).order('id desc')
  end

  # GET /test_results/1
  # GET /test_results/1.json
  def show
  end

  # GET /test_results/new
  def new
    @test_result = TestResult.new
  end

  # GET /test_results/1/edit
  def edit
  end

  def results

    @assign_patient = AssignPatient.new
    lab_id=params[:id]

    @documents = TestResult.find_by(lab_id: lab_id)

    respond_to do |format|

      format.html

      format.pdf do
        pdf = ResultsPdf.new(@documents)
        #pdf.text "HIIIII"
        send_data pdf.render, filename: "lab_results.pdf",
                                type: "application/pdf",
                                dispostion: "inline"
      end
    end
  end

  # POST /test_results
  # POST /test_results.json
  def create
    @test_result = TestResult.new(test_result_params)

    respond_to do |format|
      if @test_result.save
        format.html { redirect_to test_results_path, notice: 'Test result was successfully created.' }
        format.json { render :show, status: :created, location: @test_result }
      else
        format.html { render :new }
        format.json { render json: @test_result.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /test_results/1
  # PATCH/PUT /test_results/1.json
  def update
    respond_to do |format|
      if @test_result.update(test_result_params)
        format.html { redirect_to @test_result, notice: 'Test result was successfully updated.' }
        format.json { render :show, status: :ok, location: @test_result }
      else
        format.html { render :edit }
        format.json { render json: @test_result.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /test_results/1
  # DELETE /test_results/1.json
  def destroy
    @test_result.destroy
    respond_to do |format|
      format.html { redirect_to test_results_url, notice: 'Test result was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_test_result
    @test_result = TestResult.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def test_result_params
    params.require(:test_result).permit(:patient_id, :user_id, :status, :results, :lab_id, :service_id)
  end
end

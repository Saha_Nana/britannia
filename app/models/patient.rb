class Patient < ActiveRecord::Base

  validates :surname,  allow_blank: false,:presence => { message: "Please enter the surname"}
  validates :other_names, allow_blank: false,:presence => { message: "Please enter the surname"}
  validates :occupation, allow_blank: false,:presence => { message: "Please enter the occupation"}
  validates :address, allow_blank: false,:presence => { message: "Please enter the address"}
  validates :mobile_number,  allow_blank: false, :presence => { message: "Enter a 12 (233xxxxxxxxx) digit phone number"},
                                        # :numericality => {message: "Enter a 12 (233xxxxxxxxx) digit phone number"},
                                        :length => { :minimum => 10, :maximum => 12 }

  validates_numericality_of   :mobile_number ,
                            :only_integer => true


  validates :surname, format: { with: /\A[a-zA-Z ]+\z/,
    message: "only allows letters" }
  validates :other_names, format: { with: /\A[a-zA-Z ]+\z/,
    message: "only allows letters" }

  belongs_to :a_role, class_name: 'Role', foreign_key: :user_id

  has_many :assigned_patient, class_name: 'AssignedPatient', foreign_key: :patient_id
  
  

end

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

   belongs_to :a_role, class_name: 'Role', foreign_key: :role_id
   has_many :ass_patients, class_name: 'AssignPatient', foreign_key: :user_id
   

 # has_one :user, class_name: 'User', foreign_key: :creator_id

  # validates :username, presence: true
  # validates :user_type_id, presence: true

  def practitioner?
    role_id == 1
  end

  def lab_tech?
    role_id == 2
  end

  def nurse?
    role_id == 3
  end

  def admin?
    role_id == 4
  end

  def pharmacist?
  	role_id == 5
  end

   def receptionist?
  	role_id == 6
  end

  #        def self.per_page
  #   20
  # end

end

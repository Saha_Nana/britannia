class AllBills <Prawn::Document
  def initialize (document1)

    super( top_margin: 100)
    @bill_histories = document1
    puts "LETS SEE THE @bill_histories #{@bill_histories.inspect}"
    @bill_total = BillHistory.where(patient_id: @bill_histories[0].patient_id, status: 0)
    @total = @bill_total.sum(:price)
    puts "PRICE IS #{@total}"

    heading
    line_items
    
  end

  def heading

    text "Total Medication Bills : ", size: 10,  style: :bold
  end

  def line_items
    move_down 10

    #text "#{@bill_histories} ", size: 15,  style: :italic
   
  table line_item_rows do
    row(0).font_style= :bold
    columns(1).align= :right
    self.row_colors = ["DDDDDD","FFFFFF"]
    self.header =true
  end
   move_down 10
   puts "LETS SEE THE TOTAL #{@total}"
   text "Total Outstanding amount is GHC #{@total}", size: 15,  style: :italic
  end

  def line_item_rows
    
  
    [["Name"]+["Order"]+["Total Cost (GHC)"]] + 
    @bill_histories.map do |item|
      [item.other_names + " " + item.surname] +
      [item.service_name] + [item.price] 
       
    end
    
    
    
  end

end
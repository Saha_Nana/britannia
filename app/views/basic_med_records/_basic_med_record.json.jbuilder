json.extract! basic_med_record, :id, :patient_id, :blood_pressure, :pulse, :temp, :respiratory_rate, :user_id, :created_at, :updated_at
json.url basic_med_record_url(basic_med_record, format: :json)

json.extract! bill_temp, :id, :patient_id, :user_id, :status, :created_at, :updated_at
json.url bill_temp_url(bill_temp, format: :json)

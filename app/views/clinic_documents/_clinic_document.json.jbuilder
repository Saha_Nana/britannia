json.extract! clinic_document, :id, :patient_id, :basic_medical_record_id, :user_id, :status, :complaints, :symptoms, :vital_signs, :clinic_exams, :final_diagnosis, :investigations, :treatment_plans, :created_at, :updated_at
json.url clinic_document_url(clinic_document, format: :json)

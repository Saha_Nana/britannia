json.extract! lab_order_list, :id, :lab_name, :price, :status, :created_at, :updated_at
json.url lab_order_list_url(lab_order_list, format: :json)

json.extract! order_request, :id, :patient_id, :user_id, :personnel_id, :status, :order_master_id, :created_at, :updated_at
json.url order_request_url(order_request, format: :json)

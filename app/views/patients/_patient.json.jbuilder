json.extract! patient, :id, :title, :surname, :other_names, :mobile_number, :occupation, :address, :status, :user_id, :alt_number, :dob, :created_at, :updated_at
json.url patient_url(patient, format: :json)

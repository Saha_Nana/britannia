class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :title
      t.string :surname
      t.string :other_names
      t.string :mobile_number
      t.string :occupation
      t.string :address
      t.boolean :status
      t.integer :user_id
      t.string :alt_number
      t.date :dob

      t.timestamps null: false
    end
  end
end

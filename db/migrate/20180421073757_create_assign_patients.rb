class CreateAssignPatients < ActiveRecord::Migration
  def change
    create_table :assign_patients do |t|
      t.integer :patient_id
      t.integer :role_id
      t.boolean :status

      t.timestamps null: false
    end
  end
end

class CreateBasicMedRecords < ActiveRecord::Migration
  def change
    create_table :basic_med_records do |t|
      t.integer :patient_id
      t.string :blood_pressure
      t.string :pulse
      t.string :temp
      t.string :respiratory_rate
      t.integer :user_id

      t.timestamps null: false
    end
  end
end

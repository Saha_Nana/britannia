class CreateClinicDocuments < ActiveRecord::Migration
  def change
    create_table :clinic_documents do |t|
      t.integer :patient_id
      t.integer :basic_medical_record_id
      t.integer :user_id
      t.boolean :status
      t.string :complaints
      t.string :symptoms
      t.string :vital_signs
      t.string :clinic_exams
      t.string :final_diagnosis
      t.string :investigations
      t.string :treatment_plans

      t.timestamps null: false
    end
  end
end

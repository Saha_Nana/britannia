class CreateLabRequests < ActiveRecord::Migration
  def change
    create_table :lab_requests do |t|
      t.integer :basic_medical_record_id
      t.integer :patient_id
      t.integer :user_id
      t.integer :personnel_id
      t.boolean :status
      t.string :lab_type

      t.timestamps null: false
    end
  end
end

class CreateTestResults < ActiveRecord::Migration
  def change
    create_table :test_results do |t|
      t.integer :patient_id
      t.integer :user_id
      t.boolean :status
      t.string :results

      t.timestamps null: false
    end
  end
end

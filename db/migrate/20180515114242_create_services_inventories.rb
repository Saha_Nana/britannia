class CreateServicesInventories < ActiveRecord::Migration
  def change
    create_table :services_inventories do |t|
      t.string :service_name
      t.decimal :price
      t.integer :category_id
      t.boolean :quantifiable

      t.timestamps null: false
    end
  end
end

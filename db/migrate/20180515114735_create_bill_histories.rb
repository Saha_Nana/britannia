class CreateBillHistories < ActiveRecord::Migration
  def change
    create_table :bill_histories do |t|
      t.integer :service_id
      t.integer :patient_id
      t.decimal :price
      t.boolean :status
      t.integer :quantity
      t.decimal :unit_price

      t.timestamps null: false
    end
  end
end

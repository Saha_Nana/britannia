class CreateServiceInventoryHistories < ActiveRecord::Migration
  def change
    create_table :service_inventory_histories do |t|
      t.integer :service_id
      t.decimal :price

      t.timestamps null: false
    end
  end
end

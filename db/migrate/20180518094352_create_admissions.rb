class CreateAdmissions < ActiveRecord::Migration
  def change
    create_table :admissions do |t|
      t.integer :patient_id
      t.string :admission_note
      t.integer :user_id
      t.integer :personnel_id
      t.boolean :status

      t.timestamps null: false
    end
  end
end

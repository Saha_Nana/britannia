class CreateBillTemps < ActiveRecord::Migration
  def change
    create_table :bill_temps do |t|
      t.integer :patient_id
      t.integer :user_id
      t.boolean :status

      t.timestamps null: false
    end
  end
end

class CreateOrderMasters < ActiveRecord::Migration
  def change
    create_table :order_masters do |t|
      t.string :order_name
      t.boolean :status

      t.timestamps null: false
    end
  end
end

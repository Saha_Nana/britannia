class CreateOrderRequests < ActiveRecord::Migration
  def change
    create_table :order_requests do |t|
      t.integer :patient_id
      t.integer :user_id
      t.integer :personnel_id
      t.boolean :status
      t.integer :order_master_id

      t.timestamps null: false
    end
  end
end

class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.string :description
      t.datetime :app_time
      t.boolean :status

      t.timestamps null: false
    end
  end
end

class CreateMedicationTemps < ActiveRecord::Migration
  def change
    create_table :medication_temps do |t|
      t.string :patient_id
      t.string :service_id
      t.boolean :status

      t.timestamps null: false
    end
  end
end

class CreateLabOrderLists < ActiveRecord::Migration
  def change
    create_table :lab_order_lists do |t|
      t.string :lab_name
      t.decimal :price
      t.boolean; :status

      t.timestamps null: false
    end
  end
end

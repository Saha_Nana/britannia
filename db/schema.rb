# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180906212246) do

  create_table "admissions", force: :cascade do |t|
    t.integer  "patient_id",     limit: 4
    t.string   "admission_note", limit: 255
    t.integer  "user_id",        limit: 4
    t.integer  "personnel_id",   limit: 4
    t.boolean  "status"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "room_no",        limit: 255
    t.text     "discharge_note", limit: 65535
    t.integer  "admission_days", limit: 4
  end

  create_table "appointments", force: :cascade do |t|
    t.string   "description", limit: 255
    t.datetime "app_time"
    t.boolean  "status"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "doctor_id",   limit: 255
    t.string   "color",       limit: 255
    t.string   "patient_id",  limit: 255
  end

  create_table "assign_patients", force: :cascade do |t|
    t.integer  "patient_id",              limit: 4
    t.integer  "role_id",                 limit: 4
    t.boolean  "status"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "personnel_id",            limit: 4
    t.string   "user_id",                 limit: 255
    t.integer  "basic_medical_record_id", limit: 4
  end

  create_table "basic_med_records", force: :cascade do |t|
    t.integer  "patient_id",       limit: 4
    t.string   "blood_pressure",   limit: 255
    t.string   "pulse",            limit: 255
    t.string   "temp",             limit: 255
    t.string   "respiratory_rate", limit: 255
    t.integer  "user_id",          limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.boolean  "status"
    t.string   "complaints",       limit: 255
    t.boolean  "admission_status"
  end

  create_table "bill_histories", force: :cascade do |t|
    t.integer  "service_id", limit: 4
    t.integer  "patient_id", limit: 4
    t.decimal  "price",                precision: 10
    t.boolean  "status"
    t.integer  "quantity",   limit: 4
    t.decimal  "unit_price",           precision: 10
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "bill_temps", force: :cascade do |t|
    t.integer  "patient_id", limit: 4
    t.integer  "user_id",    limit: 4
    t.boolean  "status"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string   "category_name", limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "clinic_documents", force: :cascade do |t|
    t.integer  "patient_id",              limit: 4
    t.integer  "basic_medical_record_id", limit: 4
    t.integer  "user_id",                 limit: 4
    t.boolean  "status"
    t.string   "complaints",              limit: 255
    t.string   "symptoms",                limit: 255
    t.string   "vital_signs",             limit: 255
    t.string   "clinic_exams",            limit: 255
    t.string   "final_diagnosis",         limit: 255
    t.string   "investigations",          limit: 255
    t.string   "treatment_plans",         limit: 255
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "lab_order_lists", force: :cascade do |t|
    t.string   "lab_name",   limit: 255
    t.decimal  "price",                  precision: 10
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "lab_requests", force: :cascade do |t|
    t.integer  "basic_medical_record_id", limit: 4
    t.integer  "patient_id",              limit: 4
    t.integer  "user_id",                 limit: 4
    t.integer  "personnel_id",            limit: 4
    t.boolean  "status"
    t.string   "lab_type",                limit: 255
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "service_id",              limit: 4
  end

  create_table "medication_requests", force: :cascade do |t|
    t.integer  "basic_medical_record_id", limit: 4
    t.integer  "patient_id",              limit: 4
    t.integer  "user_id",                 limit: 4
    t.integer  "personnel_id",            limit: 4
    t.boolean  "status"
    t.string   "medication",              limit: 255
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "service_id",              limit: 4
  end

  create_table "medication_temps", force: :cascade do |t|
    t.string   "patient_id", limit: 255
    t.string   "service_id", limit: 255
    t.boolean  "status"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "order_masters", force: :cascade do |t|
    t.string   "order_name", limit: 255
    t.boolean  "status"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "order_requests", force: :cascade do |t|
    t.integer  "patient_id",      limit: 4
    t.integer  "user_id",         limit: 4
    t.integer  "personnel_id",    limit: 4
    t.boolean  "status"
    t.integer  "order_master_id", limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "service_id",      limit: 255
  end

  create_table "patients", force: :cascade do |t|
    t.string   "title",         limit: 255
    t.string   "surname",       limit: 255
    t.string   "other_names",   limit: 255
    t.string   "mobile_number", limit: 255
    t.string   "occupation",    limit: 255
    t.string   "address",       limit: 255
    t.boolean  "status"
    t.integer  "user_id",       limit: 4
    t.string   "alt_number",    limit: 255
    t.date     "dob"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "card_no",       limit: 255
  end

  create_table "radiology_requests", force: :cascade do |t|
    t.integer  "basic_medical_record_id", limit: 4
    t.integer  "patient_id",              limit: 4
    t.integer  "user_id",                 limit: 4
    t.integer  "personnel_id",            limit: 4
    t.boolean  "status"
    t.string   "radiology",               limit: 255
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "role_name",  limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "service_inventory_histories", force: :cascade do |t|
    t.integer  "service_id", limit: 4
    t.decimal  "price",                precision: 10
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "services_inventories", force: :cascade do |t|
    t.string   "service_name", limit: 255
    t.decimal  "price",                    precision: 10
    t.integer  "category_id",  limit: 4
    t.boolean  "quantifiable"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  create_table "test_results", force: :cascade do |t|
    t.integer  "patient_id", limit: 4
    t.integer  "user_id",    limit: 4
    t.boolean  "status"
    t.string   "results",    limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "lab_id",     limit: 4
    t.integer  "service_id", limit: 4
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "username",               limit: 255
    t.string   "fullname",               limit: 255
    t.string   "mobile_number",          limit: 255
    t.integer  "role_id",                limit: 4
    t.boolean  "status"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end

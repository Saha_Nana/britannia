require 'test_helper'

class BasicMedRecordsControllerTest < ActionController::TestCase
  setup do
    @basic_med_record = basic_med_records(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:basic_med_records)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create basic_med_record" do
    assert_difference('BasicMedRecord.count') do
      post :create, basic_med_record: { blood_pressure: @basic_med_record.blood_pressure, patient_id: @basic_med_record.patient_id, pulse: @basic_med_record.pulse, respiratory_rate: @basic_med_record.respiratory_rate, temp: @basic_med_record.temp, user_id: @basic_med_record.user_id }
    end

    assert_redirected_to basic_med_record_path(assigns(:basic_med_record))
  end

  test "should show basic_med_record" do
    get :show, id: @basic_med_record
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @basic_med_record
    assert_response :success
  end

  test "should update basic_med_record" do
    patch :update, id: @basic_med_record, basic_med_record: { blood_pressure: @basic_med_record.blood_pressure, patient_id: @basic_med_record.patient_id, pulse: @basic_med_record.pulse, respiratory_rate: @basic_med_record.respiratory_rate, temp: @basic_med_record.temp, user_id: @basic_med_record.user_id }
    assert_redirected_to basic_med_record_path(assigns(:basic_med_record))
  end

  test "should destroy basic_med_record" do
    assert_difference('BasicMedRecord.count', -1) do
      delete :destroy, id: @basic_med_record
    end

    assert_redirected_to basic_med_records_path
  end
end

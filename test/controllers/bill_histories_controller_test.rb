require 'test_helper'

class BillHistoriesControllerTest < ActionController::TestCase
  setup do
    @bill_history = bill_histories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bill_histories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bill_history" do
    assert_difference('BillHistory.count') do
      post :create, bill_history: { patient_id: @bill_history.patient_id, price: @bill_history.price, quantity: @bill_history.quantity, service_id: @bill_history.service_id, status: @bill_history.status, unit_price: @bill_history.unit_price }
    end

    assert_redirected_to bill_history_path(assigns(:bill_history))
  end

  test "should show bill_history" do
    get :show, id: @bill_history
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @bill_history
    assert_response :success
  end

  test "should update bill_history" do
    patch :update, id: @bill_history, bill_history: { patient_id: @bill_history.patient_id, price: @bill_history.price, quantity: @bill_history.quantity, service_id: @bill_history.service_id, status: @bill_history.status, unit_price: @bill_history.unit_price }
    assert_redirected_to bill_history_path(assigns(:bill_history))
  end

  test "should destroy bill_history" do
    assert_difference('BillHistory.count', -1) do
      delete :destroy, id: @bill_history
    end

    assert_redirected_to bill_histories_path
  end
end

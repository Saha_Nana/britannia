require 'test_helper'

class LabOrderListsControllerTest < ActionController::TestCase
  setup do
    @lab_order_list = lab_order_lists(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:lab_order_lists)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create lab_order_list" do
    assert_difference('LabOrderList.count') do
      post :create, lab_order_list: { lab_name: @lab_order_list.lab_name, price: @lab_order_list.price, status: @lab_order_list.status }
    end

    assert_redirected_to lab_order_list_path(assigns(:lab_order_list))
  end

  test "should show lab_order_list" do
    get :show, id: @lab_order_list
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @lab_order_list
    assert_response :success
  end

  test "should update lab_order_list" do
    patch :update, id: @lab_order_list, lab_order_list: { lab_name: @lab_order_list.lab_name, price: @lab_order_list.price, status: @lab_order_list.status }
    assert_redirected_to lab_order_list_path(assigns(:lab_order_list))
  end

  test "should destroy lab_order_list" do
    assert_difference('LabOrderList.count', -1) do
      delete :destroy, id: @lab_order_list
    end

    assert_redirected_to lab_order_lists_path
  end
end

require 'test_helper'

class MedicationRequestsControllerTest < ActionController::TestCase
  setup do
    @medication_request = medication_requests(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:medication_requests)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create medication_request" do
    assert_difference('MedicationRequest.count') do
      post :create, medication_request: { basic_medical_record_id: @medication_request.basic_medical_record_id, medication: @medication_request.medication, patient_id: @medication_request.patient_id, personnel_id: @medication_request.personnel_id, status: @medication_request.status, user_id: @medication_request.user_id }
    end

    assert_redirected_to medication_request_path(assigns(:medication_request))
  end

  test "should show medication_request" do
    get :show, id: @medication_request
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @medication_request
    assert_response :success
  end

  test "should update medication_request" do
    patch :update, id: @medication_request, medication_request: { basic_medical_record_id: @medication_request.basic_medical_record_id, medication: @medication_request.medication, patient_id: @medication_request.patient_id, personnel_id: @medication_request.personnel_id, status: @medication_request.status, user_id: @medication_request.user_id }
    assert_redirected_to medication_request_path(assigns(:medication_request))
  end

  test "should destroy medication_request" do
    assert_difference('MedicationRequest.count', -1) do
      delete :destroy, id: @medication_request
    end

    assert_redirected_to medication_requests_path
  end
end

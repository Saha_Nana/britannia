require 'test_helper'

class MedicationTempsControllerTest < ActionController::TestCase
  setup do
    @medication_temp = medication_temps(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:medication_temps)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create medication_temp" do
    assert_difference('MedicationTemp.count') do
      post :create, medication_temp: { patient_id: @medication_temp.patient_id, service_id: @medication_temp.service_id, status: @medication_temp.status }
    end

    assert_redirected_to medication_temp_path(assigns(:medication_temp))
  end

  test "should show medication_temp" do
    get :show, id: @medication_temp
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @medication_temp
    assert_response :success
  end

  test "should update medication_temp" do
    patch :update, id: @medication_temp, medication_temp: { patient_id: @medication_temp.patient_id, service_id: @medication_temp.service_id, status: @medication_temp.status }
    assert_redirected_to medication_temp_path(assigns(:medication_temp))
  end

  test "should destroy medication_temp" do
    assert_difference('MedicationTemp.count', -1) do
      delete :destroy, id: @medication_temp
    end

    assert_redirected_to medication_temps_path
  end
end
